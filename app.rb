require 'sinatra'
require 'json' 
require 'mechanize'
require 'pry'

$mechanize=Mechanize.new()

get '/get-orders' do
	status=params['status']
	token=params['token']
	if (status=="active" || status=="confirmed" || status=="cancelled") and (token=="4321")
		page=$mechanize.post('http://partners.mebelkart.com/vendor',{'email' => ENV['mebelkart_email'],'passwd' => ENV['mebelkart_password'],'submitSellerLogin'=>'Login'})	
		orders_summary=[]
		orders_page=$mechanize.get(get_url_to_parse(status))
	        orders_summary=parse_html(orders_page,status)
		orders_summary.to_json
	end
end


def get_url_to_parse(status)
if status=="active"
	return "http://partners.mebelkart.com/sellerActiveOrders?newOrder=1"
elsif status=="confirmed"
	return "http://partners.mebelkart.com/sellerActiveOrders?confirmedOrder=1"	
else
	return "http://partners.mebelkart.com/sellerCancelledOrders"
end
end

def parse_html(orders_page,status)
	orders=orders_page.css('tr.order_summary')
	orders_summary=[]   

	orders.each_with_index do |element,i|
		order_hash = {}
		order_html=orders[i].css('td')

		product_details=order_html[2].css('p')
		quantity_price=order_html[3].css("p")
		ordered_times=order_html[4].css("p")
		delivery_address=order_html[5].css("p")

		order_hash['sku']= product_details[0].text.split(" ").last
		order_hash['channel']="mebelkart"
		order_hash['order_id']=product_details[3].text.split(" ").last.split("/")[0]
		order_hash['status']=quantity_price[0].text.downcase
		order_hash['completed_at']=ordered_times[0].text.split(" ")[3] + " " +ordered_times[0].text.split(" ")[4]
		order_hash['first name']=delivery_address[0].text.split(" ").first
		if delivery_address[0].text.split(" ").first!=delivery_address[0].text.split(" ").last
		  order_hash['last name']=delivery_address[0].text.split(" ").last
		else
			order_hash['last name']=""
		end
		order_hash['address1']=delivery_address[1].text
		order_hash['address2']=delivery_address[2].text.split(":").last
		order_hash['city']=delivery_address[3].text.split("-").first
		order_hash['pincode']=delivery_address[3].text.split("-").last
		order_hash['state']=delivery_address[4].text
		order_hash['country']=delivery_address[5].text

		if status=="active"
	          hash = senatize_active(quantity_price, ordered_times,i)
		elsif status=="confirmed"
		  hash =senatize_confirmed(quantity_price,ordered_times)
		else
	          hash = senatize_cancelled(quantity_price,ordered_times)
		end
	        order_hash=order_hash.merge(hash)
		orders_summary.push(order_hash)
	end
	return orders_summary
end


def senatize_active(quantity_price,ordered_times,i)
	hash={}
	if i==0
	hash['quantity']=quantity_price[3].text.split(":").last
	hash['price']=quantity_price[6].text.split(" ").last
	else 
	hash['quantity']=quantity_price[2].text.split(":").last
	hash['price']=quantity_price[5].text.split(" ").last
  end
	return hash
end

def senatize_confirmed(quantity_price,ordered_times)
	hash={}
	hash['quantity']=quantity_price[3].text.split(":").last
	hash['price']=quantity_price[6].text.split(" ").last
	return hash
end

def senatize_cancelled(quantity_price,ordered_times)
	hash={}
	hash['quantity']=quantity_price[5].text.split(":").last
	hash['price']=quantity_price[8].text.split(" ").last
	return hash
end


get '/update-stock' do
	product_code=params['product_code']
	quantity=params['quantity']
	token=params['token']
	if token=="4321"
		page=$mechanize.post('http://partners.mebelkart.com/vendor',{'email' => ENV['mebelkart_email'],'passwd' =>             ENV['mebelkart_password'],'submitSellerLogin'=>'Login'})
		update_page=$mechanize.get('http://partners.mebelkart.com/sellerCatalogUpdateStock')
        	parse_html_to_get_id(product_code,quantity,update_page)
		"updated"
        end
end

def parse_html_to_get_id(product_code,quantity,update_page)
	a=update_page.css("a[title=#{product_code}]")
	id=a[0]["href"].split("/").last.split("-").first
	post_request_for_updation(quantity,id)
end

def post_request_for_updation(quantity,id)
	newpage=$mechanize.post("http://partners.mebelkart.com/ajaxfilemanager/productStockUpdates/updateStockWithSpecifiedQuantity.php",{"id_product" => id,"quantityToUpdateWith" => quantity})
end
