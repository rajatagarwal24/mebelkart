require 'sinatra'
require 'json' 
require 'mechanize'
require 'pry'

$mechanize=Mechanize.new()

get '/get-orders' do
	status=params['status']
	token=params['token']
	if (token=="4321")
		page=$mechanize.post('http://partners.mebelkart.com/vendor',{'email' => ENV['mebelkart_email'],'passwd' => ENV['mebelkart_password'],'submitSellerLogin'=>'Login'})	
		all_orders=[]
		active_orders=[]
		confirmed_orders=[]
		cancelled_orders=[]
		orders_page=$mechanize.get("http://partners.mebelkart.com/sellerOrders")
	  all_orders,active_orders,confirmed_orders,cancelled_orders=parse_html(orders_page)
		if status=="active"
			active_orders.to_json
		elsif status=="confirmed"
			confirmed_orders.to_json
		elsif status=="cancelled"
			cancelled_orders.to_json
		else
		all_orders.to_json
		end
	end
end



def parse_html(orders_page)
	orders=orders_page.css('tr.order_summary')
	all_orders=[]   
  active_orders=[]
	confirmed_orders=[]
	cancelled_orders=[]
  orders.each_with_index do |element,i|
		order_hash = {}
		order_html=orders[i].css('td')

		product_details=order_html[2].css('p')
		quantity_price=order_html[3].css("p")
		ordered_times=order_html[4].css("p")
		delivery_address=order_html[5].css("p")

		order_hash['sku']= product_details.css(".sku").text.split(" ").last
		order_hash['channel']="mebelkart"
		order_hash['order_id']=product_details.css(".order_detail").text.split(" ").last.split("/")[0]
		order_hash['status']=quantity_price.css(".current_status").text.downcase
		order_hash['completed_at']=ordered_times.css(".order_time").text.split(" ")[3] + " " +ordered_times[0].text.split(" ")[4]
		order_hash['firstname']=delivery_address.css(".customer_name").text.split(" ").first
		if delivery_address.css(".customer_name").text.split(" ").first!=delivery_address.css(".customer_name").text.split(" ").last
		  order_hash['lastname']=delivery_address.css(".customer_name").text.split(" ").last
		else
			order_hash['lastname']=""
		end
		order_hash['address1']=delivery_address.css(".address1").text
		order_hash['address2']=delivery_address.css(".address2").text.split(":").last
		order_hash['city']=delivery_address.css(".city_pincode").text.split("-").first
		order_hash['pincode']=delivery_address.css(".city_pincode").text.split("-").last
		order_hash['state']=delivery_address.css(".state").text
		order_hash['country']=delivery_address.css(".country").text
    order_hash['phone']=delivery_address.css(".phone_mobile").text.split(":").last
	  order_hash['quantity']=quantity_price.css(".quantity").text.split(":").last
    order_hash['price']=quantity_price.css(".total_price").text.split(" ").last
    all_orders.push(order_hash)
		if quantity_price.css(".current_status").text.downcase=="approved"
			active_orders.push(order_hash)
		elsif quantity_price.css(".current_status").text.downcase=="confirmed"
			confirmed_orders.push(order_hash)
		elsif quantity_price.css(".current_status").text.downcase=="cancelled" || quantity_price.css(".current_status").text.downcase=="cancelled by buyer"
			cancelled_orders.push(order_hash)
		end
	end
	return all_orders,active_orders,confirmed_orders,cancelled_orders
end

=begin
def senatize_active(quantity_price,ordered_times,i)
	hash={}
	if i==0
	hash['quantity']=quantity_price[3].text.split(":").last
	hash['price']=quantity_price[6].text.split(" ").last
	else 
	hash['quantity']=quantity_price[2].text.split(":").last
	hash['price']=quantity_price[5].text.split(" ").last
  end
	return hash
end
def senatize_confirmed(quantity_price,ordered_times)
	hash={}
	hash['quantity']=quantity_price[3].text.split(":").last
	hash['price']=quantity_price[6].text.split(" ").last
	return hash
end

def senatize_cancelled(quantity_price,ordered_times)
	hash={}
	hash['quantity']=quantity_price[5].text.split(":").last
	hash['price']=quantity_price[8].text.split(" ").last
	return hash
end
=end

get '/update-stock' do
	product_code=params['product_code']
	quantity=params['quantity']
	token=params['token']
	if token=="4321"
		page=$mechanize.post('http://partners.mebelkart.com/vendor',{'email' => ENV['mebelkart_email'],'passwd' =>             ENV['mebelkart_password'],'submitSellerLogin'=>'Login'})
		update_page=$mechanize.get('http://partners.mebelkart.com/sellerCatalogUpdateStock')
        	parse_html_to_get_id(product_code,quantity,update_page)
		"updated"
        end
end

def parse_html_to_get_id(product_code,quantity,update_page)
	a=update_page.css("a[title=#{product_code}]")
	id=a[0]["href"].split("/").last.split("-").first
	post_request_for_updation(quantity,id)
end

def post_request_for_updation(quantity,id)
	newpage=$mechanize.post("http://partners.mebelkart.com/ajaxfilemanager/productStockUpdates/updateStockWithSpecifiedQuantity.php",{"id_product" => id,"quantityToUpdateWith" => quantity})
end
